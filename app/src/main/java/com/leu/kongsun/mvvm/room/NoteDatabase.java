package com.leu.kongsun.mvvm.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.leu.kongsun.mvvm.room.model.Note;
import com.leu.kongsun.mvvm.room.dao.NoteDao;

@Database(entities = {Note.class}, version = 1, exportSchema = false) //Create database
public abstract class NoteDatabase extends RoomDatabase {
    private static NoteDatabase instance;
    /*variable for singleton, don't create multiple instance in db. */

    public abstract NoteDao noteDao();// Variable for accessing Dao operation

    public static synchronized NoteDatabase getInstance(Context context) { //Create singleton method
        if (instance == null) { //instance can be create when it's null
            instance = Room.databaseBuilder(context.getApplicationContext(), NoteDatabase.class, "note_database")
                    //create instance database
                    .fallbackToDestructiveMigration() // destruct db when version change and re-create from scratch
                    .addCallback(roomCallback) /***No need to write at first time****/
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() { // call to getInstance method
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private NoteDao noteDao;
        public PopulateDbAsyncTask(NoteDatabase db) {
            noteDao = db.noteDao();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.insert(new Note("Title 1", "Description 1", 1));
            noteDao.insert(new Note("Title 2", "Description 2", 2));
            noteDao.insert(new Note("Title 3", "Description 3", 3));
            return null;
        }
    }
}
