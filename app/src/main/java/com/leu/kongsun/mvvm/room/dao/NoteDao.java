package com.leu.kongsun.mvvm.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.leu.kongsun.mvvm.room.model.Note;

import java.util.List;

@Dao
public interface NoteDao {
    @Insert
    void insert(Note note);
    @Update
    void update(Note note);
    @Delete
    void delete(Note note);
    @Query("Delete from note_table")
    void deleteAllNote();
    @Query("select *from note_table order by priority DESC")
    LiveData<List<Note>> getAllNotes();
}
